
import sys
import requests
import argparse
from getpass import getpass


parser = argparse.ArgumentParser(description='login to pagan server and creat project.\nExemple: create--n thomas --p pagan123 http://192.168.91.128  myTarget myPRoject D:\\pagan_post\\video.mp4 ')
subparser = parser.add_subparsers(dest='command')
link = subparser.add_parser('link')
create = subparser.add_parser('create')
create.add_argument('url', type=str, help='server adress')
create.add_argument('--n', type=str, help='username')
create.add_argument('--p', type=str, help='password')
create.add_argument('target', type=str, help='Annotation Target')
create.add_argument('project_name', type=str, help='Project Title')
create.add_argument('file', type=str, help='file path')
create.add_argument('--s',type=str, help='An optional short description displayed below the automatic instructions at the beginning of the annotation. Use it to help participants understand the labelling task (max 200 characters).')
create.add_argument('--e', type=str, help='An optional short message or instructions displayed to your participants below the automatic thank you message at the end of the annotation (max 200 characters).')
create.add_argument('--type', type=str, help='Annotation type (ranktrace,gtrace,binary). ranktrace by default.')
create.add_argument('--load', type= str, help = "how to load videos (random/sequence/endless). random by default.")
create.add_argument('--survey', type=str, help = "Survey link. None by default.")
create.add_argument('--pipe_pwd', action='store_true')

link.add_argument('url', type=str, help='server adress')
link.add_argument('--n', type=str, help='username')
link.add_argument('--p', type=str, help='password')
link.add_argument('--p_name',type=str, help='find the link corresponding to the project name')
link.add_argument('--pipe_pwd', action='store_true')

args = parser.parse_args()

if args.command == 'create':
    url = args.url

    possible_type = ['ranktrace', 'gtrace', 'binary']
    possible_loading = ['random', 'sequence', 'endless']

    if args.s:
        start_message = args.s
    else: start_message = ''

    if args.e:
        end_message = args.e
    else: end_message = ''

    if args.survey:
        survey = args.survey
    else: survey = ''


    if args.type:
        type = args.type
        while(not (type in possible_type)):
            print("type must be one of the following choices : ranktrace/gtrace/binary\nPlease enter a valid type:")
            type = input()
    else: type = 'ranktrace'

    if args.load:
        load = args.load
        while(not (type in possible_loading)):
            print("loading must be one of the following choices : random/sequence/endless\nPlease enter a valid type:")
            load = input()
    else: load = 'random'

    project_data = {
        'target': args.target,
        'project_name': args.project_name,
        'start-message': start_message,
        'type': type,
        'source_type': "upload",
        'source_url[]': [''],
        'upload-message': '',
        'video_loading': load,
        'n_of_participant_runs': '1',
        'n_of_participant_uploads': '1',
        'video_sound': 'on',
        'end-message': end_message,
        'endless' : 'on',
        'survey-link' : survey,
        'autofill-id' : ''
    }


    file = args.file
    files_data_upload = {
        'file[]' : (file, open(file, 'rb'), '/video\/*/', {'Expires': '0'})
    }
    def main():
        url_log = url + '/login.php'
        url_create_project = url+ '/create_project.php'
        url_projects = url + '/projects.php'

        if not(args.pipe_pwd):
            if args.n:
                name = args.n
            else : 
                name = input("Enter your username :")

            if args.p:
                password = args.p
            else :
                password = getpass("Enter your password :")
        else:
            name = input()
            password = input()
            
        identifiants = {
        'username': name,
        'password': password,
        }

        
        with requests.session() as session:
            response = session.post(url_log,data=identifiants)
        
            
            while(("No account found with that username") in response.text):
                
                identifiants['username']= input("No account found with that username.\nEnter username :")
                response = session.post(url_log,data=identifiants)
               
            while(("The password you entered was not valid") in response.text):
            
                identifiants['password'] = getpass("The password you entered was not valid.\nEnter your passord :")
                response = session.post(url_log,data=identifiants)


            response  = session.post(url_create_project,files=files_data_upload,data=project_data)
            projects = session.get(url_projects)
            if args.project_name in projects.text:
                print("\nYour project has been successfully created!\n")
                projectLoc = projects.text.find(args.project_name)
                link = projects.text.find("annotation.php",projectLoc)
                print("Share this link with your participants:\n")
                print(url + "//" + projects.text[link:link+54])
            else :
                print("Oops an error has occured, please try again.")
elif args.command == 'link':
    
    url = args.url
    def main():
        url_log = url + '/login.php'
        url_projects = url + '/projects.php'

        
            
    
        

        if not(args.pipe_pwd):
            if args.n:
                name = args.n
            else : 
                name = input("Enter your username :")

            if args.p:
                password = args.p
            else :
                password = getpass("Enter your password :")
        else:
            name = input()
            password = input()

        identifiants = {
            'username': name,
            'password': password,
        }

        
        with requests.session() as session:
            response = session.post(url_log,data=identifiants)
        
            
            while(("No account found with that username") in response.text):
                identifiants['username']= input("No account found with that username.\nEnter username :")
                response = session.post(url_log,data=identifiants)
            while(("The password you entered was not valid") in response.text):
                identifiants['password'] = getpass("The password you entered was not valid.\nEnter your passord :")
                response = session.post(url_log,data=identifiants)

            
            projects = session.get(url_projects)
            
            if args.p_name:
                if args.p_name in projects.text:
                    
                    projectLoc = projects.text.find(args.p_name)
                    link = projects.text.find("annotation.php",projectLoc)
                    link_end = 0
                    while projects.text[link+link_end] != ('"'):
                        link_end+=1
                    print("\nHere is the link to the project named " + args.project_name +":")
                    print(url + "//" + projects.text[link:link+link_end])
                else :
                    print("Oops an error has occured, please try again.")
            else :
                balise = "<h2>"
                balise_end = "</h2>"
                project_container = projects.text.find("project-container")
                start = project_container
                while balise in projects.text[start:len(projects.text)]:
                    project = projects.text.find(balise,start)
                    project_end = projects.text.find(balise_end, project)
                    curr_project = (projects.text[project+len(balise):project_end])
                    link = projects.text.find("annotation.php",project)
                    link_end = 0
                    while projects.text[link+link_end] != ('"'):
                        link_end+=1
                    print("\nHere is the link to the project named " + curr_project +":")
                    print(url + "//" + projects.text[link:link+link_end])
                    start = project_end
                    

    
if __name__ == '__main__':
    main()
