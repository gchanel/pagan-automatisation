# Automation of project creation on PAGAN [Platform for Audiovisual General-purpose Annotation].




## Description
The main goal of this project is to automate the creation of projects on the PAGAN platform. In order to use this program you will need your own server. Here are the links for the orignal pagan site and the pagan github where you can find all the informations you need : https://pagan.davidmelhart.com/ https://github.com/davidmelhart/PAGAN

Once your server installed, you will be able to create new projects with a lot of differents options. 
Of course you could do that manually but depending on the number of projects you have to creat, it might take a lot of time.

In order to make it easier, this program allows you to create a project in a single command line.
Unfortunately, you will not be able to choose all the options avaible but you will still be able to choose different project options in this command line.

In addition to easily creating project, you will also be able to fetch the links of the projects on your server using a command line.




## Help

You can use the "-help"/"--h" option to display some help messages on how to use the script:  



pagan_post.py --h //display gobal help message  
usage: pagan_post.py [-h] {link,create} ...  

login to pagan server and creat project. Exemple:--n thomas --p pagan123 http://192.168.91.128/ myTarget myPRoject D:\pagan_post\video.mp4  

positional arguments:  
  {link,create}  

This shows that the script offers two differents command lines (create/link)  




pagan_post.py link --h //display link command line help message  
usage: pagan_post.py link [-h] [--n N] [--p P] [--p_name P_NAME] [--pipe_pwd] url  

positional arguments:  
  url              server adress  

options:  
  -h, --help       show this help message and exit  
  --n N            username  
  --p P            password  
  --p_name P_NAME  find the link corresponding to the project name (all projects by default)  
  --pipe_pwd       You can choose this option if you want to pass your username and password trough a pipeline  




pagan_post.py create --h //display create command line help message  
usage: pagan_post.py create [-h] [--n N] [--p P] [--s S] [--e E] [--type TYPE] [--load LOAD] [--survey SURVEY] [--pipe_pwd] url target project_name file  

positional arguments:  
  url              server adress  
  target           Annotation Target  
  project_name     Project Title  
  file             file path  

options:  
  -h, --help       show this help message and exit  
  --n N            username  
  --p P            password  
  --s S            An optional short description displayed below the automatic instructions at the beginning of the annotation. Use it to help participants  
                   understand the labelling task (max 200 characters).  
  --e E            An optional short message or instructions displayed to your participants below the automatic thank you message at the end of the  
                   annotation (max 200 characters).   
  --type TYPE      Annotation type (ranktrace,gtrace,binary). ranktrace by default.  
  --load LOAD      how to load videos (random/sequence/endless). random by default.  
  --survey SURVEY  Survey link. None by default.  
  --pipe_pwd       You can choose this option if you want to pass your username and password trough a pipeline  

## Usage 
Here are some usage exemples of the script:


//command to fetch a project link
python .\pagan_post.py link --n thomas --p pagan123 --p_name project1  http://192.168.91.128/    

Here is the link to the project named project1:   
http://192.168.91.128//annotation.php?id=22CAE6EB-0572-1758-92CA-A72CD8E72DAB    

If you don't give any project name, this command line shows all projects on your server.


//command to create a project
python .\pagan_post.py create --n thomas --p pagan123  http://192.168.91.128/ my_target project1 D:\pagan_post\video.mp4  

Your project has been successfully created!  

Share this link with your participants:  

http://192.168.91.128//annotation.php?id=E882E289-1783-0320-5B6A-541E5C14BCD8  

## Authors and acknowledgment

I would like to thank my supervising professor Guillaume Chanel and Dr. Marios Aristogenis Fanourakis for helping me throughout this project.
And of course many thanks to Phd. David Melhart who designed and developed  the Platform for Audiovisual General-purpose Annotation.

